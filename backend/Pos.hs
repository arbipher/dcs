module Pos where

import System.FilePath

-- the type for character positions in a file, starting from 1
type CharPos = Int

-- the type for position information
type Pos = (FilePath,CharPos)

noPos :: Pos
noPos = ("none",0)

mkPos :: String -> Int -> Pos
mkPos filename i = (filename,i)

-- we use a pair of a starting position and ending position to identify
-- expressions from the input
type Extent = (Pos,Pos)

mkExtent :: Pos -> Pos -> Extent
mkExtent sp ep = (sp,ep)

type LocalExtent = (CharPos,CharPos)

-- show a position as an argument to an elisp function call.  Only the character position is included.
elispPos :: Pos -> String
elispPos (f,p) = show p -- "\"" ++ f ++ "\" " ++ show p

-- like elispPos but include filename as well as character position
elispFullPos :: Pos -> String
elispFullPos (f,p) = "(cons \"" ++ f ++ "\" " ++ show p ++ ")"

-- incorporate an interval as arguments to an elisp function call
elispExtent :: Extent -> String -> String
elispExtent (s,e) r = " " ++ elispPos s ++ " " ++ elispPos e ++ r

showExtent :: Extent -> String
showExtent (s,e) = show s ++ "-" ++ show e

startingPosExtent :: Extent -> Pos
startingPosExtent (s,e) = s

noExtent = (noPos,noPos)

firstExtent :: FilePath -> Extent
firstExtent p = let x = mkPos p 1 in (x,x)

-- incorporate an interval as arguments to an elisp function call
elispLocalExtent :: LocalExtent -> String -> String
elispLocalExtent (s,e) r = " " ++ show s ++ " " ++ show e ++ r

