module AssocList where

import Data.List(lookup)

type AssocList a b = [(a,b)]

{- associate key with value in association list (just change first binding found) -}
assocUpdate :: Eq a => a -> b -> AssocList a b -> AssocList a b
assocUpdate key value
  ((key',value') : al) | key == key' = (key,value) : al
                       | otherwise = (key',value') : assocUpdate key value al
assocUpdate key value [] = [(key,value)]

assocLookup :: Eq a => a -> AssocList a b -> Maybe b
assocLookup = lookup

assocEmpty :: AssocList a b
assocEmpty = []