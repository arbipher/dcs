module Main where

import System.IO
import System.Environment
import Commands
import Lexer
import Parser
import Data.Text (Text,pack,unpack)
import Comments
import Syntax

initialize :: IO ()
initialize = do
  hSetEncoding stdout utf8
  hSetBuffering stdin NoBuffering
  hSetBuffering stdout NoBuffering

foreverF :: (a -> IO a) -> a -> IO ()
foreverF f seed =
  f seed >>= foreverF f

perr :: String -> String -> IO ()
perr w errMsg = putStrLn (w ++ " error at position " ++ errMsg)

main :: IO ()
main = do
  args <- getArgs
  case args of
    [] ->
      -- operate as a backend for emacs mode
      do
        initialize
        foreverF handleCmd initDcsState
    [f] ->
      do
        processFile [] f
        return ()
    _ ->
      putStrLn "Unexpected command-line arguments (run with 0 or 1)"
    

{-
perr :: String -> Text -> IO ()
perr w errMsg = putStrLn (w ++ " error at position " ++ (unpack errMsg))


main :: IO ()
main = do
  args <- getArgs
  s <- readFile $ head args
  let s' = snd $ cullComments $ s in
    case parseTxt (pack s) of 
      Left  (Left  errMsg) -> perr "Lex" errMsg
      Left  (Right errMsg) -> perr "Parse" errMsg
      Right res                    -> putStrLn "Ok" -- >> writeFile ("parser-result.ast") (show res)    

-}