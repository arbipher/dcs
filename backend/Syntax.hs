module Syntax where

import Pos
import Data.List
import Data.Tree

-- different kinds of symbols one can find in types and terms
type Var = (String,Pos)
type Const = (String,Pos)

unusedVar = "_"

varStr :: Var -> String
varStr = fst
constStr = varStr

metaVarStr :: MetaVar -> String
metaVarStr = fst

varPos :: Var -> Pos
varPos = snd
constPos = varPos

type MetaVar = (String,Extent)

-- for applying a type constant to some types
type TApp = (Const,[Ty])

-- for applying a type metavariable to some args
type TAppMeta = (MetaVar,[Ty])

----------------------------------------------------------------------
-- The constructors for the types of ASTs below will often take
-- in a list of Poss for all the punctuation
----------------------------------------------------------------------

data Arrow = ArrowPlain | ArrowFat
  deriving Eq
-- syntax for types
data Ty = Arrow [Pos] Arrow Ty Ty |
          TApp TApp |
          TAppMeta TAppMeta |
          TyParens [Pos] Ty -- Poss to keep track of where the parentheses are

-- syntax for terms
data Tm = Var Var |
          Lam [Pos] [Var] Tm |
          App Tm Tm |
          Omega [Pos] Var Var TApp Tm | -- algebras; the TApp is for the functor that is the carrier 
          Gamma [Pos] Tm [Case] | -- case splitting
          Sigma [Pos] [SigmaBinding] Tm | -- let definitions
          Parens [Pos] Tm 

data SigmaBinding = SigmaBinding [Pos] Var Tm

-- syntax for cases of gamma-terms
data Case = Case Pat Tm

-- syntax for patterns
data Pat = Pat Const [Var] -- a constructor applied to pattern variables

{- represents whether a type definition defines a functor or just a plain type definition
   The Pos is the starting position of the keyword -}
-- defining functors
data TyDef =
  TyDef [Pos] -- positions of punctuation for this construct
        Const
        [Var] {- parameters to the functor -}
        Ty {- use special variable * for functorial input -}

data DataDef =
  DataDef [Pos] {- for punctuation -}
         Const {- the defined datatype -}
         [Var] {- parameters to the datatype definition -} 
         [CtrDef] {- the constructors -}

type CtrDef = TApp

-- representing type parameters to term definitions (TmDefs)
data TyParamTm = TyParam Const | TyParamLike [Pos] {- for punctuation -} Const Ty

isTyParamLike :: TyParamTm -> Bool
isTyParamLike (TyParam _) = False
isTyParamLike (TyParamLike _ _ _) = True

data TmDef =
  TmDef
    [Pos]
    Const
    [TyParamTm] {- type parameters to the definition -}
    (Maybe Ty)
    Tm

data Statement = TyDefSt TyDef  | DataDefSt DataDef | TmDefSt TmDef

-- a File has some import directives and then a "SimpleFile" (just list of statements)
type File = (Imports,SimpleFile)
type SimpleFile = [Statement]

type Dependency = (Import,SimpleFile) -- an import statement with the body of the file it imports
type DependencyTree = Tree Dependency {- postfix left-to-right traversal of the tree is a
                                         legal linearization from needed imports to imports needing them -}
type DependencyTrees = [DependencyTree]
type FileWithDeps =
  (DependencyTrees, -- one dependency tree for each import in the main file
   SimpleFile)      -- body of the main file

type Imports = [Import]
type Path = String
data Import = Import [Pos] {- for punctuation -}
                     Path {- path for the import -}
                     Pos {- position at the end of that path -}
                     (Maybe Var) -- optional qualifier for qualified import
  deriving (Show, Eq)

dummyFile :: File
dummyFile = ([],[])

