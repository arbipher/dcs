module TyDefs where

import Pos
import Syntax
import qualified Data.Map as M

-- map binding positions of defined type constants to their definitions
type TyDefs = M.Map Pos ([Var],Ty) 

