module Highlighting where

import Util
import Pos

type Highlight = (Pos,Pos)

type Haccum a = a -> [Highlight] -> [Highlight]

showHighlighting :: String -> [Highlight] -> [String]
showHighlighting highlightTp =
  map (\ i -> "(dcs-mode-highlight-" ++ highlightTp ++ elispExtent i ")")

-- given a list of Poss for 1-character punctuation symbols, add them to a list
-- of Highlights
punctHighlighting :: [Pos] -> [Highlight] -> [Highlight]
punctHighlighting pis r = foldr (\ o r -> (o,o) : r) r pis