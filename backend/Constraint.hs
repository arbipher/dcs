{- just declare the datatype for representing subtyping constraints -}
module Constraint where

import Pos
import Syntax
import SyntaxHelpers

-- a subtyping constraint
data Constraint = Subtype Ty Ty | Like Ty Ty
  deriving Eq

showConstraint :: Constraint -> String
showConstraint (Subtype ty1 ty2) = showTy ty1 ++ " <: " ++ showTy ty2
showConstraint (Like ty1 ty2) = showTy ty1 ++ " ~ " ++ showTy ty2

instance Show Constraint where
  show = showConstraint

-- Constraints with the extent that generated them
type EConstraint = (Extent,Constraint)
