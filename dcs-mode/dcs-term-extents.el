;; -*- lexical-binding: t; -*-

;; Allow the user to select extents (regions) of the source
;; following the syntactic structure.

(defvar-local dcs-mode-current-extents
    nil
  "A list (WHICH STARTPOS INDEX ENDPOSS).
Where WHICH is a boolean indicating if we're anchored at start or
at end, STARTPOS is the starting position, ENDPOSS is the list of ending
positions, for the currently selected extents and INDEX is the next index (to
display) in that list.")

(defun dcs-mode-clear-current-extents()
  "Clear the current term extents."
  (interactive)
  (setq dcs-mode-current-extents nil))

; backend generates a call to this
(defun dcs-mode-set-current-extents(startpos endposs which)
  "Set the starting position of the currently displayed extent to be the given one, and allow user to cycle through the list of ending positions"
  (setq dcs-mode-current-extents (list which startpos 0 endposs))
  (goto-char startpos)
  (dcs-mode-show-next-of-current-extents which))

(defun dcs-mode-request-extents(pos which)
  "Request the extents for the term, anchored at the starting or ending position depending on which (should be string True or False)."
  (dcs-mode-send (concat "extents " (number-to-string pos) " " which)))

(defun dcs-mode-show-next-of-current-extents(which)
  "Show the next of the current extents, requesting these from the backend if needed."
  (interactive)
  (if dcs-mode-current-extents
      (let ((whichp (nth 0 dcs-mode-current-extents))
            (sp (nth 1 dcs-mode-current-extents))
            (i (nth 2 dcs-mode-current-extents))
            (eps (nth 3 dcs-mode-current-extents)))
        (if (null eps)
            (message "no extents")
          (let* ((l (length eps))
                 (nexti (if (< (+ i 1) l) (+ i 1) 0))
                 (previ (if (>= (- i 1) 0) (- i 1) (- l 1))))
            (if (eq (point) sp)
              ; point did not move
              (if (string= which whichp)
                  ; same anchor type
                  ; we rotate to next ending position
                  (progn
                    (setq dcs-mode-current-extents (list which sp nexti eps))
                    (dcs-mode-set-navigation-focus sp (nth i eps)))

                 ; different anchor-type
                (progn
                  ; jump to start of current extent, then request new extents at new anchor type
                  (goto-char (nth previ eps)) ; switch ends of the current extent
                  (dcs-mode-request-extents (point) which)))
            
               ; point moved
              (dcs-mode-request-extents (point) which)))))

    ; no extents currently
    (dcs-mode-request-extents (point) which)))

(defun dcs-mode-show-next-extent-anchor-start()
  "Show the next of the current extents anchored at the starting position."
  (interactive)
  (dcs-mode-show-next-of-current-extents "True"))

(defun dcs-mode-show-next-extent-anchor-end()
  "Show the next of the current extents anchored at the ending position."
  (interactive)
  (dcs-mode-show-next-of-current-extents "False"))

(provide 'dcs-term-extents)
