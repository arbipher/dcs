;; -*- lexical-binding: t; -*-

(defun dcs-mode-exit-struct-edit-before-change (x y) ; ignored
  "Helper command to exit structured editing mode before any change to the buffer"
  (interactive)
  (dcs-mode-free-edit))

(defun dcs-mode-update-modeline(is-struct-edit)
  "Helper function to update the modeline depending on whether we are in structured editing mode or free editing mode"
  (setq mode-name (if is-struct-edit "dcs!" "dcs"))
  (force-mode-line-update))

(defun dcs-mode-try-to-send-init()
  "Try to request initialization of the buffer from the backend.  The user must associate the buffer with a file first."

  (save-buffer)

  (let ((filename (buffer-file-name)))
    (dcs-mode-send (concat "init " filename))))

(defun dcs-mode-finish-init()
  "Function that the backend will invoke in its reply, to finish processing an init request"
  (dcs-mode-create-error-buffer)
)

(defun dcs-mode-struct-edit()
  "dcs mode, begin structured editing and navigation"
  (interactive)
  (message "Entering structured editing")
  (dcs-mode-update-modeline t)
  (dcs-mode-clear-highlighting)
  (dcs-mode-clear-errors)
  (dcs-mode-try-to-send-init)
  (use-local-map dcs-mode-struct-edit-map)
  (add-hook 'before-change-functions
            #'dcs-mode-exit-struct-edit-before-change
            nil t))

(defun dcs-mode-free-edit(&optional suppress-message)
  "dcs mode, begin free editing"
  (interactive)
  (unless suppress-message
    (message "Resuming free editing"))
  (dcs-mode-update-modeline nil)
  (use-local-map dcs-mode-free-edit-map)
  (dcs-mode-clear-error-focus)
  (dcs-mode-clear-current-extents)  
  (remove-hook 'before-change-functions
               #'dcs-mode-exit-struct-edit-before-change)
  (dcs-mode-clear-navigation-focus-no-rehighlighting))

(provide 'dcs-struct-edit)
