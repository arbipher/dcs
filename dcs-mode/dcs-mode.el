;;; dcs-mode.el --- Entry point for Emacs mode for dc-strong-fp (dcs)  -*- lexical-binding: t; -*-

;; Version: 0

;;; Commentary:

;; The mode can be customized with the `dcs' customization group.

;;; Code:

;; (require 'dcs-process)
;; (require 'dcs-struct-edit)
(require 'dcs-mode-library)

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.dcs\\'" . dcs-mode))
;;;###autoload
(add-to-list 'auto-coding-alist '("\\.dcs\\'" . utf-8))

(defvar dcs-mode-map
  (let ((map (make-sparse-keymap)))
    ;; It's empty for now.
    map))

;;;###autoload
(define-derived-mode dcs-mode prog-mode "dcs"
  "Major mode for editing and viewing DCS source files"
  (dcs-mode-start-process)
  (dcs-mode-free-edit t))

;;;###autoload
(defvar dcs-mode-root-list nil "List of roots registered using dcs-mode-register-root")

;;;###autoload
(defun dcs-mode-register-root (r)
  "Register the given directory as a root to give every instance of the backend, to search for imported files."
  (push r dcs-mode-root-list))

(provide 'dcs-mode)
;;; dcs-mode.el ends here
