;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; dcs keymaps
;;
;; There is one keymap for free editing, and another for structured
;; editing and navigation.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun dcs-mode-ding()
  (interactive)
  (ding))

(defvar dcs-mode-toggle-struct-edit-keystroke
  (kbd "C-c C-l")
  "Keystroke to use for toggling between structured and free editing")

(defvar dcs-mode-free-edit-map
  (let ((m (make-sparse-keymap))
        (s (make-sparse-keymap)))
    (define-key m dcs-mode-toggle-struct-edit-keystroke
      'dcs-mode-struct-edit)
    (define-key m (kbd "\\") s)
    (define-key s (kbd "o") (lambda () (interactive) (insert "ω")))
    (define-key s (kbd "g") (lambda () (interactive) (insert "γ")))
    (define-key s (kbd "r") (lambda () (interactive) (insert "→")))
    (define-key s (kbd "a") (lambda () (interactive) (insert "⇒")))
    (define-key s (kbd "l") (lambda () (interactive) (insert "λ")))
    (define-key s (kbd "d") (lambda () (interactive) (insert "δ")))
    (define-key s (kbd "s") (lambda () (interactive) (insert "σ")))
    (define-key s (kbd "t") (lambda () (interactive) (insert "τ")))
    (define-key s (kbd "i") (lambda () (interactive) (insert "ι")))
    m)
  "Keymap for dcs, free editing")

; maybe make a minor mode
(defvar dcs-mode-struct-edit-map
  (let ((m (make-sparse-keymap)))
    (define-key m dcs-mode-toggle-struct-edit-keystroke 'dcs-mode-free-edit)
    (define-key m (kbd "q") 'dcs-mode-free-edit)
    (define-key m [remap self-insert-command] 'dcs-mode-ding)
    (define-key m (kbd "1") 'delete-other-windows)
    (define-key m (kbd "z") 'dcs-mode-start-process)
    (define-key m (kbd "e") 'dcs-mode-toggle-error-buffer-window)
    (define-key m (kbd "i") 'dcs-mode-show-type-info)
    (define-key m (kbd ".") 'dcs-mode-lookup-sym-at-point)
    (define-key m (kbd "s") 'dcs-mode-show-next-extent-anchor-start)
    (define-key m (kbd "d") 'dcs-mode-show-next-extent-anchor-end)    
    (define-key m (kbd "c") 'dcs-mode-clear-navigation-focus-no-continuation)    
    (define-key m (kbd "r") 'dcs-mode-struct-edit)    
    m)
  "Keymap for dcs, structured editing and navigation")

(defvar dcs-mode-error-buffer-map
  (let ((m (make-sparse-keymap)))
    (define-key m [remap self-insert-command] 'dcs-mode-ding)
    (define-key m (kbd "n") 'dcs-mode-error-buffer-next-error)
    (define-key m (kbd "p") 'dcs-mode-error-buffer-previous-error)    
    (define-key m (kbd "e") 'dcs-mode-error-buffer-quit-window)    
    (define-key m (kbd "q") 'dcs-mode-error-buffer-quit-window)    
    (define-key m (kbd ".") 'dcs-mode-error-buffer-jump-to-error)
    (define-key m (kbd "RET") 'dcs-mode-error-buffer-toggle-details)
    m)
  "Keymap for error buffers, which displays errors reported by the backend")

(provide 'dcs-keymaps)
