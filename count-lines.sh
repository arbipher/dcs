#!/bin/bash

echo "---------------------------"
echo "backend (Haskell):"
echo
wc -l backend/*.{y,x,hs}
echo "---------------------------"
echo "frontend (elisp):"
echo
wc -l dcs-mode/*.el
