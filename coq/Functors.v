Require Import Kinds.

Definition FmapT(A B : Set)(F : Set -> Set) : Set := forall(f : A -> B), F A -> F B.
Definition FmapId(F : Set -> Set)(fmap : forall{A B : Set}, FmapT A B F) : Set :=
  forall (A : Set) (x : F A), fmap (fun x => x) x = x .

Class Functor (F : Set -> Set) :=
  {
  fmap : forall {A B : Set}, FmapT A B F;
  fmapId : FmapId F (@fmap)
  }.

Definition BimapT(A B A' B' : Set)(F : Set -> Set -> Set) : Set :=
  forall(f : A -> B)(f' : A' -> B'), F A A' -> F B B'.
Definition BimapId(F : Set -> Set -> Set)(fmap : forall{A B A' B' : Set}, BimapT A B A' B' F) : Set :=
  forall (A A' : Set) (x : F A A'), fmap (fun x => x) (fun x => x) x = x .

Class Bifunctor (F : Set -> Set -> Set) :=
  {
  bimap : forall {A B A' B' : Set}, BimapT A B A' B' F;
  bimapId : BimapId F (@bimap)
  }.


