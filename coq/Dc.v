(** * Divide and Conquer Interface *)
Require Import Kinds.
Require Import Mu.
Require Import Functors.

Require Import Coq.Logic.FunctionalExtensionality.

Section Dc.
(** Assumptions *)

  Variable F : Set -> Set.
  Context {FunF : Functor F}.

Definition F2 (R P : Set) := F P.

Definition bimapF2{A B A' B' : Set}(f1 : A -> B)(f2 : A' -> B')(xs : F2 A A') : F2 B B'.
  unfold F2.
  apply (fmap f2).
  exact xs.
Defined.

Definition bimapIdF2 : BimapId F2 (@bimapF2).
  intros A A' x.
  unfold bimapF2.
  apply fmapId.
Defined.

Global Instance BifunF2 : Bifunctor F2 :=
  { bimap := @bimapF2 ;
    bimapId := bimapIdF2 }.

(** Helper Typedefs *)

Definition FoldT(alg : KAlg)(C : Set) : Set :=
  forall (X : Carrier) (BifunX : Bifunctor X), alg X -> C -> X C C.

(** Subsidiary Algebra *)

Definition SAlgF
           (A: KAlg)
           (X : Carrier) : Set
  := forall
       (P : Set)
       (R : Set)
       (up : R -> P)    
       (sfo : FoldT A R)    
       (abstIn : F R -> P)      
       (rec : R -> X R R)      
       (d : F R),             
       X R P.

Definition SAlg := MuAlg SAlgF.

Definition monoSAlg : forall (A B : KAlg), CastAlg A B -> CastAlg (SAlgF A) (SAlgF B) :=
  fun A B castSAlg =>
    fun X salgf P R cRS sfo =>
      salgf P R cRS (fun X1 xmap y => sfo X1 xmap (castSAlg X1 y)).

Definition rollSAlg : forall {X : Carrier}, SAlgF SAlg X -> SAlg X :=
  fun X d => inMuAlg SAlgF d.

Definition unrollSAlg : forall {X : Carrier}, SAlg X -> SAlgF SAlg X :=
  fun X d => outMuAlg SAlgF monoSAlg d.

Definition castSAlgId : forall (A : KAlg), CastAlg A A :=
  fun A X d => d.
  
(* fmapId law for HO KAlg Functor *)
Lemma monoSAlgId :
  forall (A : KAlg) (X : Carrier) (salgf : SAlgF A X),
    monoSAlg A A (castSAlgId A) X salgf = salgf.
  intros.
  unfold monoSAlg.
  repeat (apply functional_extensionality_dep; simpl; intros).
  repeat f_equal.
Qed.                                                  

(** Regular Algebra *)

Definition AlgF(A: KAlg)(X : Carrier) : Set :=
  forall (R : Set)
      (fo : FoldT A R)
      (sfo : FoldT SAlg R)
      (rec : R -> X R R)      
      (d : F R),             
      X R R.

Definition Alg := MuAlg AlgF.

Definition monoAlg : forall (A B : KAlg), CastAlg A B -> CastAlg (AlgF A) (AlgF B) :=
  fun A B f =>
    fun X algf R fo  =>
      algf R (fun X xmap alg => fo X xmap (f X alg)).

Definition castAlgId : forall (A : KAlg), CastAlg A A :=
  fun A X d => d.
  
(** fmapId law for higher order [KAlg] Functor *)
Lemma monoAlgId :
  forall (A : KAlg) (X : Carrier) (algf : AlgF A X),
    monoAlg A A (castAlgId A) X algf = algf.
  intros.
  unfold monoAlg.
  repeat (apply functional_extensionality_dep; simpl; intros).
  repeat f_equal.
Qed.

Definition rollAlg : forall {X : Carrier}, AlgF Alg X -> Alg X :=
  fun X d => inMuAlg AlgF d.

Definition unrollAlg : forall {X : Carrier}, Alg X -> AlgF Alg X :=
  fun X d => outMuAlg AlgF monoAlg d.

Lemma UnrollRollIso :
  forall (X : Carrier) (algf : AlgF Alg X), unrollAlg (rollAlg algf) = algf.
  intros.
  apply monoAlgId.
Qed.

(** building Dc, our initial algebra carrier *)

Definition DcF(C : Set) := forall (X : Carrier) (FunX : Bifunctor X), Alg X -> X C C.
Definition Dc := Mu DcF.
    
Definition fmapDc(A B : Set) : FmapT A B DcF := fun f initA => fun X xmap alg => bimap f f (initA X xmap alg).

Lemma fmapIdDc : FmapId DcF fmapDc. 
  intros A x.
  unfold fmapDc.
  apply functional_extensionality_dep.
  intro X.
  apply functional_extensionality_dep.
  intro bifunX.
  apply functional_extensionality_dep.
  intro alg.
  apply (@bimapId X).
Qed.

Instance initFunc : Functor DcF :=
  {
  fmap := fmapDc;
  fmapId := fmapIdDc
  }.
  
Definition rollDc: DcF Dc -> Dc :=
  inMu DcF.

Definition unrollDc: Dc -> DcF Dc :=
  outMu (FunF := initFunc) DcF.

Definition fold : FoldT Alg Dc := fun X BifunX alg d => unrollDc d X BifunX alg.

Definition RevealT(X : Carrier) : Carrier := (fun R1 R2 => (R1 -> Dc) -> (R2 -> Dc) -> (X R1 Dc)).

Definition RevealBimap(X : Carrier)(Bifun:Bifunctor X)(A B A' B' : Set)(f : A -> B)(f' : A' -> B')
                     (xs : RevealT X A A') : RevealT X B B'.
  intros r1 r2.
  unfold RevealT in xs.
  apply (bimap f (fun x => x)) .
  apply xs.
  intro a.
  exact (r1 (f a)).
  intro a'.
  exact (r2 (f' a')).
Defined.

Lemma RevealBimapId(X : Carrier)(Bifun : Bifunctor X)(A A' : Set)(xs : RevealT X A A'):
         RevealBimap X Bifun A A A' A' (fun x => x) (fun x => x) xs = xs.
  unfold RevealBimap.
  apply functional_extensionality_dep.
  intro r1.
  apply functional_extensionality_dep.
  intro r2. 
  apply bimapId.
Qed.

Global Instance BifunRevealT(X : Carrier)`(Fun:Bifunctor X) : Bifunctor (RevealT X) :=
  { bimap := RevealBimap X Fun ;
    bimapId := RevealBimapId X Fun }.

Definition promote : forall (X : Carrier)
                            (BifunX : Bifunctor X),
    (SAlg X) -> Alg (RevealT X).
  intros X bifunX salg.
  apply rollAlg.
  intros R fo sfo rec fr r1 r2.
  set (abstIn := (fun fr => rollDc (fun X bifunX alg =>
                                           bimap r1 r2 (unrollAlg alg R fo sfo (fo X bifunX alg) fr)))).
  set (rec' := sfo X bifunX salg).
  apply (unrollSAlg salg Dc R).
  - exact r1.
  - exact sfo.
  - exact abstIn.
  - exact rec'.
  - exact fr.
Defined.

Definition sfold : FoldT SAlg Dc :=
  fun X funX salg x =>
    fold (RevealT X) (BifunRevealT X funX) (promote X funX salg) x (fun x => x) (fun x => x).

Definition out{R : Set}(sfo : FoldT SAlg R) : R -> F R :=
  sfo F2 BifunF2 (rollSAlg (fun _ _ up _ _ _ d => fmap up d)).

Definition inDc : F Dc -> Dc :=
  fun d => rollDc (fun X xmap alg =>
                    unrollAlg alg Dc fold sfold (fold X xmap alg) d).
  
End Dc.

Arguments rollAlg{F}{X}.
Arguments rollSAlg{F}{X}.

