ι /Stdlib/Basic/Bifunctors
ι /Stdlib/Data/Nat

half : Nat ⇒ There =
  ω half(x) : There .
    γ x {
      Zero → Zero
    | Succ x' →
      γ x' {
        Zero → Zero
      | Succ x'' → Succ (half x'')
      }
    }

toZeroHalf : Nat ⇒ K Nat =
  ω toZeroHalf(x) : K Nat .
    γ x {
      Zero → Zero
    | Succ x → toZeroHalf (half x)
    }

iter (R ~ Nat) : (R → R) → R → Nat ⇒ K R =
  λ f a .
    ω iter(n) : K R .
      γ n {
        Zero → a
      | Succ x → f (iter x)
      }

toZeroIterId : Nat ⇒ K Nat =
  ω toZeroIterId(x) : K Nat .
    γ x {
      Zero → Zero
    | Succ x → toZeroIterId (iter (λ x . x) x x)
    }
